"""Scrapes images from a 4chan thread."""
import os
import urllib
import urllib.request
from urllib.parse import urlparse
import json
from bs4 import BeautifulSoup
from tqdm import tqdm

def check_url(url):
    url_parsed = urlparse(url)
    if not (url_parsed.scheme == 'http' or url_parsed.scheme == 'https'):
        print("Invalid protocol.")
        return False

    #first check, make sure it is a 4chan url
    if not (url_parsed.netloc == "boards.4chan.org"):
        print("Not a valid 4chan url.")
        return False

    #split the path to check it is a valid thread path
    path_parsed = url_parsed.path.split("/")
    if(len(path_parsed) > 4 or len(path_parsed) < 4):
        print("Not a valid thread path.")
        return False

    if(path_parsed[1] == "" or path_parsed[2] != "thread" or path_parsed[3] == ""):
        print("Not a valid thread path.")
        return False


def scrape_thread(raw_url):
    """Takes a thread url and checks to make sure it is valid, before downloading the images"""
    if check_url(raw_url) is False:
        return
    url_parsed = urlparse(raw_url)
    path_parsed = url_parsed.path.split("/")

    board = path_parsed[1]
    thread_number = path_parsed[3]
    json_url = "https://a.4cdn.org/" + board + "/thread/" + thread_number + ".json"

    #finally, assuming the address met all criteria, try and load it
    try:
        website = urllib.request.urlopen(json_url).read()
    except:
        print("No valid JSON address found.")
        return

    save_location = os.path.dirname(os.path.realpath(__file__)) + "\\scrape\\" + board + "\\"
    if not os.path.exists(save_location):
        print("Making image directory...")
        os.makedirs(save_location)

    formatted_json = json.loads(website)
    num_items = len(formatted_json['posts'])
    print("Downloading files...")
    for i in tqdm(range(0,num_items)):
        try:
            json_tim = formatted_json['posts'][i]['tim']
            json_ext = formatted_json['posts'][i]['ext']
            if os.path.isfile(save_location + str(json_tim) + json_ext):
                print("File '" + save_location + str(json_tim) + json_ext + "' already exists, skipping.")
            else:
                urllib.request.urlretrieve("https://i.4cdn.org/" + board + "/" + str(json_tim) + json_ext, save_location + str(json_tim) + json_ext)
        except KeyError:
            pass

    print("Done. Press enter to quit...")
    input()

def main():
    """Main entry point"""
    url = input("Enter a 4chan thread url: ")
    scrape_thread(url)

if __name__ == "__main__":
    main()